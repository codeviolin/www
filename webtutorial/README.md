# Web tutorial
___

Introduction:

HTML, JavaScript, and CSS are the languages of the web. All the websites you use are made up of a minimum of three files: one for HTML, one for JavaScript, and one for CSS. This tutorial will teach you the  CSS and HTML portions, which are the two [front-end languages](https://en.wikipedia.org/wiki/Front-end_web_development) that make up the web. Essentially, in relation to a person, HTML makes up the skeleton, JavaScript makes up the muscle, and CSS makes up the clothes. This is a comprehensive HTML and CSS tutorial. After completing all the lessons and projects, you should be able to create a website with HTML and CSS on your own.

Please don't copy and paste the examples. It's only hurting you.

## Lesson 1: Tags

*Tags* are the basic syntax item in HTML. Tags such as `<p>`, `<head>`, `<body>`, `<nav>`, `<h1>`, `<h2>`, etc. are all essential to the HTML document. To signal that the tag is finished, you must use a *closing tag* which is just the opening tag plus a forward slash, like this: `</p>`.

Fire up your editor and setup your `index.html` file with the following tags:


```
<!doctype html>
<html>
<head>

</head>
<body>

</body>
</html>
```

With the resources provided to you, either set up a server using [browsersync](https://www.browsersync.io), with your native [REPL.it](https://repl.it) server (not recommended) or sourcing [GitLab](https://www.gitlab.com) through [Netlify](https://www.netlify.com). 
Next, add a `<p></p>` tag to your website in between the two `<body>` tags. Type something in between the `<p>` tags. Save, then check out your server.
Now, add a heading to your website just above the `<p>` tag: `<h1></h1>`. Type something in the heading, save, then return to your server.
Next, you want to add a link to another website. The `<a>` (anchor) tag is used for this. Type `<a href="LINKEDWEBSITEADDRESSHERE"></a>`. In place of the 'LINKEDWEBSITEADDRESSHERE', add a web address (e.g. https://www.google.com). Now, go to your website and you will find the link.

These are just some of the basic tags in HTML. You will learn more later.


### Lesson 1.1: Links

Links can also have text displayed. Do this by typing the replacement text between the `<a></a>` tags.
Then, check out your website.

### Lesson 1.2: Images

Images are one of the rare tags that don't require a closing. This is called *self-closing*. Place an image in the body with an `<img>` tag:

```
<img src="https://i.imgur.com/vlmFtun.png">
```

Use your own image.

### Lesson 1.3: Headings

Headings are the titles of HTML documents. Headings come in different forms: `<h1>`, `<h2>`, `<h3>`, etc. The larger the number, the smaller the header.

Add an `<h2>` subtitle below the old `<h1>` title. Close it with `</h2>`.

### Lesson 1.4: Title

Use the `<title>` tag to give the browser a title to display in your website's tab. Put the `title` in the `head` of your document, like this:

```
<head>
<title>CodeViolin</title>
</head>
```


### Congratulations! You now have a basic HTML document.

## Lesson 2: Identifiers and Properties

Identifiers and properties are essential in the styling and readability of an HTML document. *Identifiers* allow you to reference the tag in CSS and JavaScript files, and *Properties* give life to the page.

The `src` property used in the `img` tag gives the tag its image. Identifiers can include `id`, `class`, `alt`, and more. Properties include `src`, `href`, `height`, `width`, and more. These are all necessary to an HTML document.

Add a `width` property to the `<img>` tag with a value of `400px`. (px = pixels):

```
<img src="://i.imgur.com/vlmFtun.png" width="400px">
```

This will give the image a width of 400 pixels.

Other units of measure include `em`, `rem`, and `%` of the screen.

Add an `id` to the `<p>` element:

```
<p id="paragraph">This is a paragraph</p>
```


This will come in useful when creating a CSS stylesheet.

Practice adding other identifiers and properties to your tags.

## Project: Informative One-Page Website

Now you have learned enough to build a simple website.

Set up your website like before. Now, choose a topic to do research on for the website. After you've done that, begin your website! Make it informative.

You must use a minimum of the following elements: 1 `<h1>` tag, 2 `<h2>` tags, 4 `<p>` tags, 1 `<a>` tag, and 1 `<img>` tag. You must also use three different properties and identifiers of your choice. 


* Note: If the website doesn't look good, that's not your fault! HTML naturally has a very basic-looking style. CSS will allow us to style our pages how we want them.

## Lesson 3: Multiple pages

Now that we have one page, why don't we make another? Rename the first file to `home.html`. Next, create another file in the same directory (folder) named `blog.html`. At the top of `home.html`, make a link to `blog.html`:

```
<a href="blog.html">Blog</a>
```

Next, on `blog.html`, make a link to `home.html`.

Add a blog post with a header to your new page about anything from politics to the TV show you watched (and hated) last week.

Open your server and test the link.

Now, create as many pages as you would like and link to them.

## Lesson 4: Semantics

Semantics give meaning to your page. While *presentational* elements give your website material, *semantic* elements give it structure, like navigation bars and footers. [Semantic HTML](https://www.w3schools.com/html/html5_semantic_elements.asp) is necessary for the organization and user-interface (UI) of your website.

Semantic tags:


`<article>` = an article, such as a blog post or book review.

`<aside>` = a sidebar, sometimes containing navigation or a button to return to the top of the page

`<details>` = additional details that the user can view or hide

`<figcaption>` = caption for a table, video, graph, image, etc.

`<figure>` = self-contained content including tables, videos, graphs, images, etc.

`<footer>` = content below the main content, often containing copyrights and contact information

`<header>` = content above the main content, often containing a page title

`<main>` = the main content, where most of the text is

`<mark>` = marked or highlighted text

`<nav>` = the navigation bar

`<section>` = defines a section in the document

`<summary>` = defines a heading for a `<details>` element

`<time>` = defines the date or time


Add semantic tags to both of your pages in order to help you categorize your content. You'll thank me as your page continues to grow.

## Lesson 4: CSS Basic Syntax

CSS is a file made up of different styles for different objects in your page, all of which are written by you. Before we learn the different style options and how to program them into your site, start by making a file: `main.css` OR `styles.css`.

You can use the same tags in HTML (i.e. `<p>`, `<nav>`, etc.) as CSS identifiers:

```
p {

}
```

Add the `p` section to your CSS file.

Inside of that you will place all of your styles that are *unique* to the `<p>` element:

```
p {
  font-family: "Times New Roman";
  font-size: 14px;
  color: grey;
}
```
Here is a list of common CSS styles that are used:

```
font-family

background-color

color

overflow

font-size

text-align

font-style

width

height

display

text-decoration

background

position

float
```

### Lesson 4.1: The Box Model

![The Box Model](boxmethod.gif)

The Box Model is a CSS system for identifying the structure of a website. In the above image, the center region is a part of your website (e.g. a figure). Then, the padding between the content and the border keeps the figure away from the border. The border defines the outside edge of the item, and the margin keeps the item away from the other items on the page.

The Box Model also applies to your main content. Apply padding to a `body` index in your CSS file to pad the sides of the body and the top. Use the three units of the box model to better organize your website:

```
body { 
  padding:  ;
  border:  ;
  margin:  ;
}

```

### Lesson 4.2: Units of Measure

In CSS, there are three main units of measure:

1. EM: A larger unit of measure.
1. PX: 1 pixel of the screen.
1. %: Percent of the screen. Size changes when screen size changes.

These units can be used on anything from `font-size` to `padding`.

### Lesson 4.3: Semantics for CSS

Here we come across another reason that semantic tags should be used: in order to better identify items in CSS. Each individual semantic can have its own style, so the more semantics you use, the easier it will be to create a dynamic CSS stylesheet.

### Lesson 4.4: Using identifiers in CSS

Another way to give specific styles to items is through identifiers.

Use the `id` of an item like this:

```
#id {
  color: green;
}
```

Use the `class` of an item like this:

```
.class {
  color: green;
}
```

One of the few times I use a Google tool is when I'm looking for fonts. In general, built-in CSS fonts are pretty basic. [Google Fonts](fonts.google.com) is a good source for fonts of all forms.

### Lesson 4.5: Linking to your CSS stylesheet

Add the following line to the `<head>` of your HTML document to link to your stylesheet (note: must be in every document using the stylesheet:

```
<link rel="stylesheet" type=text/css" href="main.css"
```

## Project: Dynamic multiple-page styled website

Your assignment: create a website with at least 3 pages, a navigation bar, links, images, and a stylesheet.Give the website your best. Make it fun, informative, entertaining, or, best of all, all three! Use CSS to your advantage to give the website a style and a theme. Design a logo at [piskel](piskelapp.com). Have fun.

Congratulations on completing this tutorial! 
