Article I: Obstruction of Justice

In his conduct of the office of President of the United States, Richard M. Nixon, in violation of his constitutional oath faithfully to execute the office of President of the United States, and, to the best of his ability, preserve, protect, and defend the Constitution of the United States, and in violation of his constitutional duty to take care that the laws be faithfully executed, has prevented, obstructed and impeded the administration of justice, in that:

On June 17, 1972, and prior thereto, agents of the Committee for the Re-election of the President:

Committed unlawful entry of the headquarters of the Democratic National Committee in Washington, District of Columbia, for the purpose of securing political intelligence. Subsequent thereto, Richard M. Nixon, using the powers of his high office, engaged personally and through his subordinates and agents in a course of conduct or plan designed to delay, impede, and obstruct the investigation of such unlawful entry; to cover up, conceal and protect those responsible; and to conceal the existence and scope of other unlawful covert activities. ...

Wherefore Richard M. Nixon, by such conduct, warrants impeachment and trial, and removal from office.

Article II: Abuse of Power

Using the powers of the office of President of the United States, Richard M. Nixon, in violation of his constitutional oath faithfully to execute the office of President of the United States, and to the best of his ability preserve, protect and defend the Constitution of the United States, and in disregard of his constitutional duty to take care that the laws be faithfully executed, has repeatedly engaged in conduct violating the constitutional rights of citizens, impairing the due and proper administration of justice in the conduct of lawful inquiries, of contravening the law of governing agencies of the executive branch and the purposes of these agencies. ...

Article III: Defiance of Subpoenas

In his conduct of the office of President of the United States, Richard M. Nixon, contrary to his oath faithfully to execute the office of President of the United States and, to the best of his ability, preserve, protect, and defend the Constitution of the United States, and in violation of his constitutional duty to take care that the laws be faithfully executed, has failed without lawful cause or excuse to produce papers and things as directed by duly authorized subpoenas issued by the Committee on the Judiciary of the House of Representatives on April 11, 1974, May 15, 1974, May 30, 1974, and June 24, 1974, and willfully disobeyed such subpoenas. ...

 