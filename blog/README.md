# Blog
___

## Opinion: Imminent War

This week, tensions between the United States and Iran have escalated a thousandfold. In the middle of the week, pro-Iranian demonstrators laid siege to a US embassy in Iraq, prompting retaliation: Thursday night, from his Mar-A-Lago resort in Florida, President Trump ordered a successful airstrike on Iranian Major General Qasem Soleimani in a Baghdad airport. Soleimani was the leader of the elite Quds Force, with nearly limitless financial and militaristic resources. He was also a household name in Iran, and essentially the second most powerful person in Iran.

This attack warrants almost certain Iranian retaliation. The assassination was an unapproved act of war. Could these events lead to World War III? Yes. World War I began on the basis of the assassination of Franz Ferdinand, the Archduke of Austria-Hungary at the time. He was the heir presumptive of Austria-Hungary. Qasem Soleimani was believed by many to be the likely next leader of Iran. Is it possible that his importance can lead to war? If so, could this war escalate into global war? Yes and yes, but the outcome is by no means inevitable. If war was declared, Iran could proceed to attack Israel. Israel would be defended by the US, UK, Germany, France, Canada, among other countries, and opposed by others. A conflict of this magnitude would likely turn nuclear after the US' withdrawal from the Iran nuclear deal in May 2018. What are the consequences of global nuclear war? Nothing good. 


## TRUMP IMPEACHED ON TWO ARTICLES

December 18, 2019. Remember the date. It was the day on which a president became the third president ever to be impeached by the House of Representatives and the first ever to be charged with Obstruction of Congress. The vote in the house was split heavily along partisan lines, with no Republican congressperson breaking with Trump and only 3 (Article I) and 4 (Article II) Democratic congresspersons breaking ranks with Speaker Pelosi. The 45th President, Donald J. Trump, is officially accused of Abuse of Power and Obstruction of Congress by the House:

Article I.: Abuse of Power (230-197)

The House found considerable evidence of President Trump leveraging foreign aid and presidential power to coerce the Ukraine into providing dirt on his potential opponent in the 2020 presidential election, former Vice President Joe Biden. In a now infamous July 25 phone call with Ukraine's newly elected President Volodymir Zelenskyy, he asked for Zelenskyy to 'do us a favor' and that he needed Mr. Zelenskyy to 'look into [some 'bad stuff' that happened with Joe Biden's son Hunter Biden stopping 'the prosecution']' and assuring Mr. Zelenskyy that former US ambassador to the Ukraine Marie Yovanovitch 'would go through some things.' Ms. Yovanovitch later testified that she felt pressure from Mr. Trump to leave her post. Later reports show that the US withheld necessary military aid that was needed to help the Ukraine defend against Russian aggression as well as a White House meeting between Mr. Trump and Mr. Zelenskyy until Mr. Zelenskyy agreed to provide the investigations against Mr. Biden.

Article II.: Obstruction of Congress (229-198)

The House also found that Mr. Trump obstructed the House's impeachment inquiry by blocking witnesses from testifying and violating subpoenas.

The Articles will be passed to the Senate for an impeachment trial whenever Speaker Pelosi sees fit to do so.


## The road to becoming a terminal master.

*Terminal mastery* is the term for the mastery of the tools, functions, and environments that allows a person to quickly manage a server, develop a website, write a progressive web app, or write an application. These tools can allow you to program, code, or maintain up to 10x faster. In order to become a terminal master, it is impertinent that you learn to use these tools, which include:

* Vi/Vim: The terminal text editor. Built into every terminal, everywhere (except on Windows). Allows you to navigate through a document exponentially faster than with competing editors including VSCode and Atom.
* TMUX: The ultimate multitasking window. Allows you to open or run multiple different applications, editors, and functions at the same time, without ending the process if you need to go. Allows you to edit multiple files side-by-side, everywhere.
* Lynx: The terminal web browser. Runs without a JavaScript script and provides an entirely text-based browsing option.

Learn more about becoming a terminal master in the [tutorial](/terminalmastery/), coming soon.

## The future of quantum supercomputing is here.

Google has announced that it has [achieved](https://www.nature.com/articles/s41586-019-1666-5) quantum supremacy. This means that it has built a quantum supercomputer that, as they claim, has completed the execution of a program that would take a standard supercomputer 10,000 years to complete. Quantum computing adheres to the standard principles of quantum mechanics. It employs qubits, which can be set to the values of 1, 0, or *both*. This simple yet profound fact allows the supercomputer to process exponentially more information at a time than a standard supercomputer. An advanced concept in quantum mechanics, the state of *quantum entanglement*, entails two particles that have become physically entangled with each other, meaning that they each are in each other's place as well as their own at the same time, until observed. This breakthrough is essentially the digitalized form of quantum entanglement. Quantum computing is the future. Are you ready?

![A quantum supercomputing qubit. Source is Hackernoon](https://hackernoon.com/hn-images/0*TyQVCk7_d17Ns7Ln.png)



## Trump's base has left him.

Trump once [celebrated](https://www.usatoday.com/story/news/politics/2019/07/25/fox-news-poll-trump-economy/1825176001/) a Fox News poll. This newest one isn't looking good for him:

![People surveyed in a stunning Fox News poll want Trump impeached and removed at a 51% rate.](https://a57.foxnews.com/static.foxnews.com/foxnews.com/content/uploads/2019/10/1862/1048/8ff3ab71-1.png?ve=1&tl=1)

Now remember, this is **FOX NEWS** we're talking about. Generally their polls would be more right-leaning. Often, they wouldn't even be willing to publish a poll like this.

To some people, this probably looks good for the president. Only 51% of people want him impeached and removed? That's good judging by the recent scandal around him. However, this is absolutely TERRIBLE for Trump. Fox News viewers are often diehard, brainwashed, ignorant supporters of Donald Trump's far-right agenda. If this many of his supporters have turned against him, this could potentially turn into a "stand with us or stand against us" mantra pushed to multiple red-state senators - vote to remove and be re-elected, or vote against removal and lose your next election.

Of course, you've got to see the Donald's side of this:

> From the day I announced I was running for President, I have NEVER had a good @FoxNews Poll. Whoever their Pollster is, they suck. But @FoxNews is also much different than it used to be in the good old days. With people like Andrew Napolitano, who wanted to be a Supreme Court Justice & I turned him down (he’s been terrible ever since), Shep Smith, @donnabrazile(who gave Crooked Hillary the debate questions & got fired from @CNN), & others, @FoxNews doesn’t deliver for US anymore. It is so different than it used to be. Oh well, I’m President!

The biggest thing we have to draw from this is the last sentence: "Oh well, I'm President!" This is a blatant misunderstanding of what it means to be president: to Trump, it means power and glory.

## Above the law

And we have another post about the POTUS. He clearly doesn't know [how the impeachment process works](/blog/#finally), and thinks that he's above the law. He thinks that by investigating him, Speaker Nancy Pelosi and House Judiciary Committee Chair Adam Schiff, whom he calls "Nervous Nancy" and "Liddle' Adam Schiff":

> Nancy Pelosi knew of all of the many Shifty Adam Schiff lies and massive frauds perpetrated upon Congress and the American people, in the form of a fraudulent speech knowingly delivered as a ruthless con, and the illegal meetings with a highly partisan "Whistleblower" & lawyer...

The president then goes on to insinuate that the two aforementioned members of the House 'evilly "Colluded"' with others and that all involved must be 'immediately Impeached!'.

> ....This makes Nervous Nancy every bit as guilty as Liddle' Adam Schiff for High Crimes and Misdemeanors, and even Treason. I guess that means that they, along with all of those that evilly "Colluded" with them, must all be immediately Impeached!

1. This is ridiculous
2. Impeachment is a *check and balance* method *written in the Constitution*.
3. Donald Trump himself released the "rough transcript" of the call and the whistleblower complaint. Why didn't anyone in the White House claim that the whistleblower complaint was "fake" before [September 27](https://twitter.com/realDonaldTrump/status/1177579380819845120)? His story keeps changing.
4. He used the words Shifty and Liddle' again to describe Adam Schiff. This is a plainly anti-semitic insult to  Schiff, the chairman of the House Judiciary Committee.

## Liddle'

The President of the United States made a *Liddle'* spelling error today-and then stood by it. 

> Liddle’ Adam Schiff, who has worked unsuccessfully for 3 years to hurt the Republican Party and President, has just said that the Whistleblower, even though he or she only had second hand information, “is credible.” How can that be with zero info and a known bias. Democrat Scam!

The President of the United States can't spell 'little.' what else is new? He also thinks that apostrophes are hyphens and that the media is the 'LameStream Media':

> To show you how dishonest the LameStream Media is, I used the word Liddle’, not Liddle, in discribing Corrupt Congressman Liddle’ Adam Schiff. Low ratings @CNN purposely took the hyphen out and said I spelled the word little wrong. A small but never ending situation with CNN!

This vast misunderstanding of the English language and punctuation was not overlooked by Merriam-Webster:


> For those looking up punctuation early on a Friday morning: A hyphen is a mark - used to divide or to compound words. An apostrophe is a mark ' used to indicate the omission of letters or figures.

I am tired of the ignorance shown daily by the president. But I will forever use 'Liddle' instead of 'Little' from now on. That's just how he influences you.

A sidenote:

<img src="https://pbs.twimg.com/media/EFegdv0X4AAUq8i.jpg:large" id="letter">

<figcaption>Michael Cohen's letter to Fordham University President Joseph M. McShane on his grades: "Don't share the President's grades with the public."</figcaption>


## Investigation unnecessary.

House Democrats who have moved to impeach President Trump in the past two days are beginning an investigation into the President's actions. As you can read [below](/blog/the-3-most-ridiculous-quotes-from-trumps-speech-during-the-912-democratic-debate), Trump *already* had committed multiple impeachable offenses that he had admitted to or had been proven *before* the Ukraine scandal. Now that the White House has released the transcript of the phone call between the President and Ukraine president Volodymyr Zelenskyy, impeachment should be a given rather than a choice. Pelosi made the right decision. Now it's time to get senate Republicans on board with the conspiracy and remove President Trump from office. But what happens if Mike Pence becomes President? Pence could reasonably win a re-election campaign. He is an intelligent, logical candidate (despite his far-right views) that could convince undecided voters to flip right. This is something that President Trump likely will not do. But what if the Senate votes to keep Trump in office (i.e. Mitch McConnell does his thing)? This scenario could legitimately boost Trump's approval rating by quite a bit, even to his first positive rating [since January 31, 2017](https://projects.fivethirtyeight.com/trump-approval-ratings/?ex_cid=rrpromo). This boost could reasonably cause a re-election - so is all this really a good idea?

The answer is yes. "No one is above the law," Speaker Pelosi said. "This is a betrayal of his oath of office." This much is certainly true. Leverage against his political opponents? Personal financial gain? Suppression of freedom of the press? [Insulting a child?](https://www.washingtonpost.com/politics/2019/09/24/response-greta-thunberg-is-reminder-that-no-trump-critic-is-immune-attacks-by-presidents-supporters/) That last one is not an impeachable offense, but it is certainly something that the President should be ashamed of.

In conclusion, President Trump wants to be impeached. Speaker Pelosi believes he needs to be impeached. And Moscow Mitch is gonna side with Trump.


## Finally!

Speaker of the House Nancy Pelosi today consented to a formal impeachment inquiry against President Trump, as allegations about coercion of Ukraine continue to develop. The speaker called a meeting with her party and formally announced that proceedings would move forward after that meeting. As you can see from the below post, this could (and should) have happened earlier. President Trump is a president who deserved to be impeached before he came into office. Now, this could actually happen.

Here's the rundown: To remove a president from office through impeachment, the following must happen:

1. Articles of impeachment must be written by House committees.
1. House must take a vote on the impeachment articles and the articles must win by a majority.
1. President must stand trial in front of members of the House (prosecutors) and Senate (jury), with the Chief Justice of the Supreme Court presiding over the proceedings.
1. Senate must vote in favor of conviction with a 67% supermajority.
1. The President, Vice President, and "all civil Officers of the United States" may be impeached and removed from office if convicted of "Treason, Bribery, or other high Crimes and Misdemeanors." (i.e. maladministration, abuse of power, etc.)

#impeachnow

## It's time to stop obstructing justice.

Grounds for impeachment? We're far past that. There is an entire pile of reasons that the president should be impeached and then immediately prosecuted for Obstruction of Justice. Nixon's impeachment articles can be found [here](/blog/impeachment.md). Trump's violations include and go beyond those three articles. Trump **violated subpoenas** in blocking two witnesses from appearing before the House Judiciary Committee, and strictly limited [Corey Lewandowski](https://www.washingtonpost.com/opinions/2019/09/17/best-evidence-obstruction-justice/)'s testimony. This is **obstruction of justice** by itself, and forget when Trump instructed Michael Cohen to lie before Congress. Trump **abused power** when he shut down the government for 35 days over border wall funding. This cost taxpayers and government employees thousands of dollars each, while government employees had to work without pay. This is explicitly holding the American people and the country hostage while Trump tried to get what he wanted. Trump has also violated the [Emoluments clause](https://www.washingtonpost.com/business/what-you-need-to-know-about-the-emoluments-clause/2019/09/17/f0b2ad9c-d975-11e9-a1a5-162b8a9c9ca2_story.html), as he uses his businesses to gain money from foreign officials and diplomats. He has stated that he would like the 2020 G7 to be held at Mar-a-Lago. He has also tried to [throw journalists in jail](https://www.cnn.com/2017/05/22/politics/trump-comey-jail-journalists/index.html), which is a direct violation of the First Amendment. These are just 5 grounds that could be used to remove President Trump from office.


## The 3 most ridiculous quotes from Trump's speech during the 9/12 Democratic Debate

President Trump made a speech during the September 12 #DemDebate. Here are the 3 most ridiculous things he said:

* (suggesting that a house uses wind energy as its sole supplier of energy) "You happen to be watching the Democratic Debate and the wind isn't blowing. You're not going to see the debate. 'Charlie, what the hell happened to the debate?' He says, 'Honey, the wind isn't blowing.' The goddamned windmill stopped."

This is a complete misunderstanding of the way renewable wind energy works. No house has a windmill that powers it directly - the energy is *stored* for future use.

* "Whether you like me or not, it doesn't matter. You have to elect me; you have no choice. A Democratic president would take your money and very much hurt your families."

Yeah, like all the multimillionaires' and billionaires' families are going to be hurt by extreme income taxing and wealth taxes.

* "I always look orange, and so do you."

Speaking about energy-efficient lighting, with his administration having already tried to rollback Obama-era energy-efficiency lightbulb regulations, Trump railed against the cost of the bulbs and said that their light was "no good." "If it breaks down, it's a hazardous waste site. It's gasses inside." Just like the White House will be when you leave office.


## Fake Weather

So Trump now has 5 major categories in which to place his statements: fake news, false accusations, misinformed suggestions, derogatory comments/rants, and *fake weather*. He first [tweeted](https://twitter.com/realdonaldtrump/status/1168174613827899393?s=12) about his fake weather statements, then covered up the ignorance with a media interview that involved him displaying a board with the projected track of Hurricane Dorian extended to include Alabama:

![The Trumpian Weather Board](https://gossiponthis.com/wp-content/uploads/2019/09/donald-trump-hurricane-dorian-map.jpg)
Then, after Trump caused panic in Alabama, the Birmingham NWS had to clean it all up with [this tweet](https://mobile.twitter.com/nwsbirmingham/status/1168179647667814400) that, under normal circumstances, would have been outrageously unnecessary (but this was ***CLEARLY NOT*** a normal situation). 

How long will the situation in the White House be allowed to continue? Trump works on his own agenda for the benefit of himself and the upper class, while neglecting the other 90% of the country. In other words, Trump is working against the good of the country.

## Best Web Browser alternatives to Google Chrome

3. Safari

Safari for iOS gets a revamp with iOS 13, and therefore it's not entirely worthless like it used to be. But access to the browser needs to be protected with FaceID or TouchID, and that's not (yet) a feature. If that becomes an option, Safari may go up on the list.  Upsides: simple browser with sleek design and excellent UI. Downside: only available on iOS, iPadOS, tvOS, and macOS.

2. Microsoft Edge

Edge has been a favorite of mine for a while - and now comes with an added bonus: Chromium. With full support for all Google Chrome extensions and features, Edge is an ideal replacement for the average Chrome user. Upsides: Excellent privacy services. Downsides: Only has support on Microsoft desktop and laptop computers, as well as Xbox 1 and beyond, but also has full support for iOS and Android devices, meaning that someone could use Edge on their iPhone, but then not be able to sync (a feature) to another device.

1. Brave Web Browser

Brave is the best cross-platform web browser replacement for Chrome out there. Built with Chromium and with full support of all Google Chrome extensions and features, complete privacy features (tracker, phishing, and full HTTP to HTTPS upgrades) AND with outstanding DevTools (for the developer who has been using Chrome for just that). Install Brave on all of your devices and sync in a heartbeat.

Install Brave at [brave.com](https://brave-browser.readthedocs.io/en/latest/installing-brave.html#), [the App Store (iOS)](https://apps.apple.com/us/app/brave-fast-privacy-browser/id1052879175), or [Google Play (not recommended)](https://play.google.com/store/apps/details?id=com.brave.browser).



0. Lynx

Lists start at zero and this one ends at zero. Lynx is the ultimate quick web browser for the developer (and don't look it up to try and download it-it's a terminal app. Read more about Lynx [here](https://www.codeviolin.me/blog/#use-lynx-terminal-web-browser).


## Apple's New iPhone design is just ugly (and not worth buying).

Apple has designed the iPhone XI with a triple lens camera hub (that protrudes approx. 5 mm from the rest of the phone), no FaceID, and no 3D touch. Basically, iPhone just ruined iPhone. Look at the design:




![[iPhone XI Designs](https://www.91mobiles.com/hub/iphone-11-first-live-images-leak-design-triple-camera/)](iphone11designs.jpeg)



Pick up your iPhone XR from the store while you still can, and wait for iPhone 12 to come out - 'cause that camera hub is just plain ugly.



## When Democracy Dies

"So this is how democracy dies - with thunderous applause." Sorry to use a *Star Wars* quote, but it seems that George Lucas was looking into 2019 back in 2005. The corrupt GOP, with Donald Trump at the helm, seems to be going out of their way to kill democracy - President Trump has joked that he could extend his term by two years because (and I quote) "they have stolen two years of my (our) Presidency (Collusion Delusion). The Witch Hunt is over but we will never forget. MAKE AMERICA GREAT AGAIN!" Complete nonsense in that Muller did not specify that there *wasn't* collusion, just that he hadn't found any evidence of collusion. However, Muller did find that the Russian hacker group Fancy Bear [hacked](https://www.buzzfeednews.com/article/sheerafrenkel/meet-fancy-bear-the-russian-group-hacking-the-us-election) the 2016 election. The GOP has oppressed and dehumanized the press, House Democrats, and all opposition through Fox News and has illegally obstructed justice (as proven by the Muller Report). Trump could be indicted for obstruction of justice the second he leaves office, but because he is the sitting president, he [cannot be prosecuted while in office](https://www.washingtonpost.com/posteverything/wp/2017/06/08/sitting-presidents-cant-be-prosecuted-probably/?noredirect=on&utm_term=.c3d64eeb9d1b). This seems to place the president above the law, which is a violation of the Constitution. Therefore, this immunity seems to be the door to a dictatorship. Hence, democracy dies. 


## Download iOS 13 Today

iOS 13, the biggest makeover of the iPhone since iPhone, can be downloaded today practically bug-free! Here's how:

1. Sign up to be a beta tester at https://beta.apple.com/sp/betaprogram/.
1. Select your device from the menu, then click the link that says 'Enroll Your iOS/macOS/iPadOS/tvOS Device'. Then, back up your device to iTunes or to iCloud. Instructions on iTunes backup will be on the page. To backup to iCloud, enter Settings -> Apple ID -> iCloud -> iCloud backup and toggle it on, then click 'Back Up Now'.
1. From your iOS device, go to beta.apple.com/profile to download the configuration profile.
1. Open Settings, tap the profile and follow the onscreen instructions.
1. Then, perform a software update to iOS 13 Public Beta.
1. DARK MODE!!!

EDIT: iOS 13 has been fully released for the public. DO NOT follow these steps. Perform a standard software update at Settings -> General -> Software Update.

## Privacy-violating Privacy Policies

[Google](https://www.zdnet.com/article/google-bought-my-friends-face-for-5/#ftag=RSSbaffb68), Amazon, and Facebook are all major companies with major privacy violations. For a moment, click the Google link above. Can we just take a moment to think about how serious that is? OK, let's continue. Google allows any person to place encroaching web trackers on their website for free - and this can be easily modified to mean any person could easily turn Google into malware. Amazon uses web trackers to suggest 'other items' for you to buy that are 'similar' to yours. They scam you into purchasing Prime for the amazing video service (it's nothing special). Facebook literally gathers your friend information, your websites visited, your online activity, etc., and sells it to companies like Cambridge Analytica. These companies then illegally post political advertisements tailored to your data. These 'free' services intended to make life easier are actually not free - you are selling your privacy to use them.

[Goooooooogle.com](http://goooooooogle.com/) is *intended* to be a search engine that is just like Google but instead doesn't use ads or trackers. I beg to differ. Here is a paragraph copied directly from their website:

'Goooooooogle.com search is a customized google.com search.  Our aim is to use Google’s own customization to improve the results for our audience.  Goooooooogle.com (Eight O’s) users are typically between 8 and 22 years old.  They use cell phones and social networking and do not like pointless ads.  If this is you, please make us your home page and primary search engine.  You will not be disappointed.  OK, that was a bit of an arrogant statement, I apologise.  To be honest, you may well be disappointed.  I mean, we are not revolutionizing search or redefining data streams here.  It is just our idea of improved results, less cluttered.  If you like it tell your friends.  If you think it is stupid, ask your friends for their opinion and tell us too.' 

Solutions:

1. Install the DuckDuckGo privacy essentials extension. This will block ad trackers.
1. NEVER USE GOOGLE, EVER AGAIN! This includes no YouTube, Google Maps, Gmail, any Android smartphone, Chrome, etc.(Exceptions include developers that need to use Chrome for the DevTools extension and have installed the DuckDuckGo Privacy Essentials Extension.)
1. On an iPhone, open settings and go to Privacy -> Advertising and toggle on 'Limit Ad Tracking'. This will prevent third-party apps from tracking you.
1. On any mobile device, download the DuckDuckGo app and use it rather than Google. See a list of useful !bangs [here](https://duckduckgo.com/bang).
1. Delete your Facebook account and then enter your Settings -> Privacy -> Advertising and click 'Reset Advertising Identifier'. This will prevent you from getting personal ads.
1. Use the Brave Browser on any laptop or desktop computer. This is built with chromium and supports Chrome extensions, but actually respects your privacy.
If you like getting relevant ads, sobeit. Just know the dangers.
1. Get a [ProtonMail](protonmail.com) account. There's your replacement for Gmail.
1. Unfortunately, there's not a very good replacement for YouTube. Running YouTube in Brave or DuckDuckGo should be OK.
1. Trade in your Android Smartphone for an Apple one. Unfortunately, Androids are the only smartphones currently running Linux, so that will be a sacrifice.
1. Use Apple Maps instead of Google Maps. (iOS 13 comes with a huge improvement to Apple Maps).







## Chess Variations

### Pawn Football

Pawn Football is played with only pawns and a king. Promote a pawn or take all your opponent's pawns to win.

### Take Me

The first person to lose all of their pieces - wins! If a piece can be captured, it must be captured. 

### Bughouse

A four player game, Bughouse includes two pairs of partners with opposite colors. When one captures their opponent's piece, they give it to their partner who can place one piece (this counts as a turn) on any unoccupied square of the board. Pawns cannot be placed on the first or eighth rank.

### Deny

A regular game of chess - but here's the catch: on each turn, your opponent may deny one of your moves. Therefore, you must have two good moves in store for them - and you must get checkmate twice.

### Hanging

Ever leave a piece unprotected accidentally where it can be captured? That's a *hanging* piece. In this game, any piece left hanging is a game changer: it literally ends the game. Leave a piece hanging and you're toast.

### Bomb 

At the beginning of the game, secretly add a red dot to the bottom of any of your pieces (other than the king). In place of moving on a turn, the player may overturn their bomb piece. All adjacent pieces of both colors (including diagonal) are removed from the board, including the bomb carrier. If the king is blown up, the opposing team wins. Otherwise, checkmate wins. If the bomb carrier is captured, the bomb is defused and the piece cannot explode. 

### Einstein

In Einstein chess, every time a non-capturing move is made, the piece is demoted into the next smallest unit. Every time a capturing move is made, the piece is promoted into the next biggest unit. Order: Pawn-Knight-Bishop-Rook-Queen-King. Pawns cannot promote on the last rank or be demoted. Queens cannot be demoted. Kings cannot be demoted or promoted. 

### Fischer Random Chess

Named for grandmaster Bobby Fischer, in this game the initial setup of pieces (not pawns) is random. 

Procedures:

* Pawns are placed on the second rank.
* Draw random pieces from a bag, and place them on A1, B1, C1, D1, etc. 
* All white pieces (not pawns) are placed on the first rank.
* The white king must be in between the two white rooks.
* The white bishops must occupy two different-colored squares.
* The black pieces are then placed to mirror the white pieces. 


### Progressive

On each turn, a player will gain two more moves than the last turn they had. For example, white moves once, then black moves twice, then white moves thrice, then black moves four times, etc. Play to checkmate or stalemate.

### King

#### Invented by me!


In this game, kings move like queens, but still must be protected. Kings can move, come back, and still castle if the rook hasn't moved. Queens move like kings. There is no check or checkmate: the king must be captured to win.

### Power

#### Invented by me!

Each piece has a new ability:

* King can move one adjacent pawn (following the rules of piece movement) and then move to that square when in check.
* Queen can jump over one piece per turn. 
* Rook can choose to move like a knight, but not to capture.
* Bishop can choose to become the piece it captures, but cannot become a pawn.
* Knights in the way of queens cannot be jumped.
* Pawns can move diagonally to promote, even when not capturing. En passant can happen anytime pawns are side-by-side, but not when promoting.




## Top Six Card/Board Games 

Stop playing video games and join your family in these fantastic Card/Board Games (in no particular order):

### Chess

Chess: The Strategy Game. With no luck involved, it pits your brain against your opponent's in a battle of strategy and tactics. Move your pawns, knights, bishops, rooks, and the queen to protect your king and defeat the other. This classic board game is a must-play.

[Purchase Here](https://www.amazon.com/QuadPro-Magnetic-Travel-Folding-Educational/dp/B06XXGPNML?SubscriptionId=AKIAILSHYYTFIVPWUY6Q&tag=duckduckgo-d-20&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B06XXGPNML)

### Exploding Kittens

The #1 most backed kickstarter ever, Exploding Kittens is all about playing cards to prevent yourself from exploding. At the end of each turn, draw a card and hope it's not an Exploding Kitten. If it is, you must play a defuse card. During your turn, you can play various cards including 'See The Future', 'Favor', 'Shuffle', 'Attack', and 'Skip'. This game is a great family game and takes approximately 15 minutes to play.

[Purchase Here](https://www.amazon.com/Exploding-Kittens-LLC-EKG-ORG1-1-Card/dp/B010TQY7A8/ref=sr_1_2?keywords=exploding+kittens&qid=1562780961&s=gateway&sr=8-2)

### Coup

Coup is an excellent strategy game. You receive two roles (out of a possible five) and use the abilities they give you to eliminate your opponents' role cards. The catch: the game is all about bluffing your roles. Keep a poker face, or all may be lost.

[Purchase Here](https://www.amazon.com/Indie-Boards-Cards-Dystopian-Universe/dp/B00GDI4HX4/ref=sr_1_1?keywords=coup&qid=1562787237&s=gateway&sr=8-1)

### Munchkin

Battle monsters, collect treasure, and choose to help or hinder your opponent in this card combat game. Amass energy points to boost your odds against monsters and defeat your foes to take loot. Kill ten monsters to win, but keep tabs on your opponents: they might sweep in and steal the win.

[Purchase Here](https://www.amazon.com/Steve-Jackson-Games-1408SJG-Munchkin/dp/1556344732/ref=sr_1_9?keywords=munchkin&qid=1562787282&s=gateway&sr=8-9)


### Avalon

Avalon, the game of deception and loyalty, takes place in a time when disloyalty runs awry. King Arthur has amassed many enemies over the years, and Merlin is fighting to protect him. Each player receives a character and some knowledge about who the other players are. The players, split into the 'Loyal Servants of Arthur' and the 'Minions of Mordred' venture on five quests that determine who triumphs - and who is defeated. The catch: the 'Assassin' (a Minion of Mordred) may guess the identity of Merlin. If she is correct, Mordred wins the day.

[Purchase Here](https://www.amazon.com/Resistance-Avalon-Social-Deduction-Game/dp/B009SAAV0C/ref=sr_1_3?keywords=avalon&qid=1562787329&s=gateway&sr=8-3)

### Dungeons and Dragons

The stereotypical 'nerd' game, D&D pits players against foes in the roleplay brain game. The dungeon master tells a story and the other players must use their powers to defeat all that stands in their way. Get an imaginative and experienced Dungeon Master and be prepared to spend a few hours when playing!

[Purchase Here](https://www.amazon.com/Dungeons-Dragons-Starter-Wizards-Team/dp/0786965592/ref=sr_1_5?keywords=dungeons+and+dragons&qid=1562787373&s=gateway&sr=8-5)


## Top Five Things To Do in Acadia National Park

### Park Loop Road

The stunning 27 mile road takes you past all the great tourist attractions: Cadillac Mountain, Sieur de Monts Spring, the Wild Gardens of Acadia, Sand Beach, Thunder Hole, Otter Point & Cove, Little Hunters Beach, Jordan Pond, and the Bubbles. Stopping at all these places will result in a lucrative half day to full day (depending on time spent per stop), and is especially worth it if you only have one day in Acadia.

### Bar Harbor

This quaint town will introduce you fully to Maine, and it is a good destination for a first day. With a variety of shops, restaurants, and parks, this town has everything to offer. Top 5 Stops in Bar Harbor: Ben & Bill's Chocolate Emporium, Geddy's Restaurant, the Shore Path, the Village Green, and the Rock and Art Shop. Even if you are only in Acadia for a day, check out Bar Harbor.

### Wonderland/Ship Harbor

These two short-but-sweet hikes give an exhilarating view of the rocky Maine coastline. Lots of standing water (Mosquitoes) inhibits the experience, but making at least one of these two hikes is worth it.


### Cadillac Mountain

The 1530 foot peak, tallest in Acadia, is certainly worth visiting. The summit is marked by an oversized medallion. When the fog rolls in, this summit will give an amazing above-the-clouds view. On the clearest days, you can see all the way to Camden Hills State Park, over 1 hour and 30 minutes by car. See sunset at the Blue Hill Overlook.

![Cadillac Mountain in Dense Fog](Cadillac.JPG)

### Jordan Pond and the Bubbles

The iconic Jordan Pond provides excellent views from the south shore. Park at the designated 'Jordan Pond Path' parking and spend a few minutes soaking in the beauty of the pond from the shore. Then, take the right (eastern) path. In about half an hour, you will reach the 'South Bubble to Bubble Rock' trailhead. This strenuous trail is not for the faint of heart, but provides excellent views and takes you directly to Bubble Rock. Warning: make sure you have found Bubble Rock. The glacial erratic is usually crowded from June through October, so a rock without a crowd isn't it. From the parking lot to the top of the South Bubble is about 1.7 miles hike.


![The South Shore of Jordan Pond](jpatb.JPG)

![Jordan Pond From the South Bubble](sbjp.JPG)

![Bubble Rock](bubblerock.JPG)

![Jordan Pond in the Mist](jpm.JPG)




## Use Lynx Terminal Web Browser

Much more secure, much faster, and much easier to operate - if you're a Terminal Master. Know the controls of Vi/Vim before venturing into the land of Lynx configuration and use.

### In a bash shell, type:

```
vimtutor
```

to learn how to use the Vim editor, and complete the tutorial.

### Install Lynx:

in the bash shell with

```
sudo apt install lynx
```

### Configure Lynx:

Copy the following files into your repository. (Download links)

* [lynx.cfg](lynx.cfg)
* [lynx.lss](lynx.lss)
* [rc](rc)
    
Add the following lines to the bottom of the rc file:

```
alias lynx="lynx -cfg=$LOCATIONOFLYNX.CFG/lynx.cfg -lss=$LOCATIONOFLYNX.LSS/lynx.lss"

    
export EDITOR=vi
```

### 'Location of the file' example:

```
/repos/codeviolin/www
```

![Lynx Terminal Web Browser (VS Code)](lynx.png)

I highly recommend adopting lynx as your primary web browser.


## Incognito, Incogschmieto

Don't believe a word they say - *it's a trap!* I'm talking about Google Chrome, of course. Supposedly, Incognito mode protects you from malicious prying eyes, and allows your privacy to be yours. On the contrary:

![Incognito ("Private") Mode](incognito.png)

This is just another example of the privacy violations at Google, and the way we're going, identity thieves are going to have easy access to the info. The sharing of private information and data by way of insecure pathways through cyberspace makes Google users' data a prime target for hackers. I advise avoiding Google Chrome and other Google products whenever possible, and using Firefox and Microsoft Edge when available (*and [DuckDuckGo](https://duckduckgo.com)*) - though Safari will do as well.

On a mobile device, download the DuckDuckGo app:

[App Store](https://itunes.apple.com/us/app/duckduckgo-search-stories/id663592361?mt=8)

And, if you must: 
[Google Play](https://play.google.com/store/apps/details?id=com.duckduckgo.mobile.android)

Remember, don't use Google unless you must.

## You Should Read Shakespeare Before High School

The works of Shakespeare are so valuable to cognitive ability and thought processes, making you think while reading, rather than just reading for fun. Don't get me wrong - reading for fun is great, and we should do it more. But reading Shakespeare for the first time should happen in middle school. The infinite number of references to the plays will be completely confusing if you haven't read the full works. Students that can read and comprehend raw Shakespeare - or even half-comprehend it - in middle school will be far better off when reading it in high school, just because of the familiarity and experience with the language and plot style.


## More Idiocy? Seriously?

Yes. Seriously.
So President Trump thinks placing tariffs on the Mexican government, violating his USMCA, is a good idea and will 'put pressure on the government of Andres Manuel Lopez Obrador to block illegal immigrants from entering the US.' I have to hope he's wrong. Let me get one thing clear: this country is BUILT ON IMMIGRANTS. Christopher Columbus, abusive of power as he was, was an IMMIGRANT (kind of?)! So to all the white people in this country that are descended from Europeans and are supporting President Trump, that makes you against yourself! It is a paradoxical issue. Supporting Trump is what you think is right, to keep all these alleged 'aliens' (they're not) out, but in truth you are descended from immigrants. You wouldn't be alive if they hadn't sought something here in America. So stop supporting President Trump's aims.

[Source](https://www.wsoctv.com/news/politics/trump-says-tariffs-on-mexico-will-stem-illegal-immigration/954502215)

## Google Chrome

When I try to open the DuckDuckGo privacy blog ([https://spreadprivacy.com](https://spreadprivacy.com)), Google Chrome gives me an unexpected warning about:

### Your connection is not private

##### Attackers might be trying to steal your information from www.spreadprivacy.com (for example, passwords, messages, or credit cards).

This is a blatant lie. Actually, it is a true self-contradiction. While Google tries to steal your information for their "analytics," DuckDuckGo works to promote internet privacy, safety, and anonymity worldwide. I truly suggest you read their blog (use Firefox if Chrome doesn't let you) and take their sincere messages to heart.

## Google

So Google wants to steal our information but they don't want other people to steal it... I guess they feel ownership towards our personal information with their "send reports to google so that we can analyze it" when they are really just stealing your identity ([Google Text Adventure](https://techcrunch.com/2018/10/01/theres-a-secret-text-adventure-game-hidden-inside-google-heres-how-to-play-it/)). I wish some of those people who are designing things like Google could not be scumbags and instead be like the people who designed [DuckDuckGo](https://duckduckgo.com) ([DuckDuckGo Privacy Blog](https://spreadprivacy.com/)). The moral of the story: stop using all Google products except when you have need for Chrome DevTools (a necessary feature).

## Google, Part 2
[Google Analytics](https://analytics.google.com) is just plain horrible. Oh, by the way: I don't suggest clicking on that link. I don't need to say much except that they ([Google](https://www.google.com)) recently started collecting receipts for people that have read a document. Basically, your business is now everyone else's. Also, they didn't tell users about the new feature, so you have to search around for the setting to turn it off (if you're interested, click [here](/analyticsoff/)). I have to ask again: who collects personal information of users, shares it with other users, and then advertises it as something good? And why does everyone fall for it?

## Fish and Education

Are amphibians fish? This question came up in my Mathematics One class, making me question the intelligence of my peers. But why aren't people well educated? Is it the fault of our Science teachers? I think not. Our principals? No again. The district? Possibly, but I think that the most reasonable answer is the state and the DOE. It is not the question of the quality of our teachers, but their motivation level. For some teachers, they are self-motivated to teach well and their horrific paychecks are just another roadblock that they'll get through. But for others, (To quote a recent TIME article) "My child and I share a small bed in a one room apartment, spend $1000 dollars on supplies, and I've been laid off three times due to budget cuts. I'm a teacher in America." Now, hear me out. While a teacher might make $2880/month ($18 per hour) (North Carolina), A professional YouTuber might make $1 million/month (denote that the teacher salary per month is assuming they are paid for 40 hr/week, while the YouTuber might work 3 hours in the week).

So back to the original query. Amphibians are not, in fact, fish, or vice versa. The 4 major animal groups on Earth include: Mammals (endothermic, animals with hair and backbones), Reptiles (ectothermic, (birds, snakes, etc)),  Amphibians (ectothermic), and Fish (ectothermic).


## Rice

<img src="https://28oa9i1t08037ue3m1l0i861-wpengine.netdna-ssl.com/wp-content/uploads/2015/03/Rice.png" id="rice-house" alt="rice-house">
<figcaption>Rice House</figcaption>

I came across this image and it was interesting to me that one grain of rice for everyone in the world would take up less space than a house for a middle-class person living in the US. Sad, not interesting. Sad to know that some people in the world are living off of one bowl of rice a day. Sad to know that the work [not even really work, but physical labor] being done by some of these farmers pays off so lightly. Sad to know that billions of people are living in starvation while only thousands are living lavishly.

* [http://freerice.com/category](http://freerice.com) is a website that donates 10 grains of rice to charities for those starving for every question correct (choose your category!)
* "A world will not survive morally or economically when so few have so much and so many have so little."


## Vaccines
These stupid, stupid people that are against vaccines! Here's some logic:

> There are more vaccinated kids with autism than unvaccinated kids

Guess what? There are also more vaccinated kids.

> I choked once when I was a kid so I'm not going to let my kids have food to protect them from choking.


![More logic](https://static.boredpanda.com/blog/wp-content/uploads/2018/06/anti-vaxxer-vaccination-facts-funny-comebacks-1-5b2a1e4e500f8__700.jpg)

![And some more](https://static.boredpanda.com/blog/wp-content/uploads/2018/06/5b28b864f0213_3sy0x4u8r0wz__700.jpg)

Source: [Bored Panda](https://www.boredpanda.com/anti-vaxxer-vaccination-facts-funny-comebacks/?utm_source=google&utm_medium=organic&utm_campaign=organic)


## Standardized Tests

Standardized tests are an extreme evil. They control students' lives and hinder learning capacity. Imagine what we could do, what we could become, if we didn't stop learning in lieu of review with a month left in the school year. Also, the sense of finality for the final week of the year forces everyone into a week of "fun." I suggest we remove tests for the system's sake, for right now, the system is broken.


## The Problem With Twitter Isn't Trump. It's That We Care About Twitter.

[The Atlantic](https://www.theatlantic.com/ideas/archive/2019/04/political-leaders-should-stop-caring-about-twitter/588004/)
<br>
Parents talk about just ignoring bullies - if they get attention, they will continue their behaviors. It's no different here. Trump is just a big bully who wants attention. If we ignore him, either he'll stop or he will have to actually stop watching Fox and Friends and tell the country what's going on instead of tweeting it. At least that way he will burn some calories. Anyway, Trump needs to quit tweeting - he basically represents everything that is wrong with social media. While everyone else gets reprimanded for minor things, he's just able to post personal threats whenever he wants without warning? Actually, I propose that Twitter blocks President Trump from usage whatsoever, as he is a toxic presence on the site. Hopefully next he'll start using Facebook.
