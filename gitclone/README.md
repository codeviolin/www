# Git Cloning and Netlify Sourcing
___

1. Open a new [GitLab](https://gitlab.com) or [GitHub](https://github.com) window.
1. Create a new project.
1. Click the blue button that says "Clone," then click "Clone with SSH."
1. Open a terminal.
1. Paste the clone address that you copied.
1. It will clone an empty directory.
1. Use `git add .`, `git commit`, and `git push` to send your files to git.

Sending to Netlify:

1. Open a new [Netlify](https://www.netlify.com) window.
1. Sign up for Netlify.
1. Click "New site from Git".
1. Click the provider of your Git repository.
1. Log in to the git provider and choose your repository.
1. Choose your preferred settings.
