# Log:

## Sept. 18-25:

* Created a live, bootable Ubuntu ISO that can boot on any Linux machine
* FreeCodeCamp JavaScript: Through 'Escape Sequences in Strings'
* Explored Discord Bots and potential Summercamp curriculum
* Website ([blog](/blog/), [tutorial](/linuxiso/))
