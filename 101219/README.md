# Carowinds
___

Attractions:

* Carolina Goldrusher
* Hurler
* Kiddy Hawk
* Ricochet
* Woodstock Express
* Boo Blasters on Boo Hill
* Rock N’ Roller
* WindSeeker?
* The Grand Carousel
* PEANUTS Pirates
* Carolina Skytower


Dining:

* Harmony Hall - multiple options (Italian, BBQ, Mexican, American)

Park opens at 10am, closes at 5pm

