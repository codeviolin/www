# Escape Room

Verbal Instructions:

Solve a physical escape room to find a flashdrive.

Flashdrive contains files that you need to access.

Flashdrive has one file that any computer can access.

You access it with a laptop (LC1)

All the other files on this flashdrive can only be accessed by another computer that has no display.

You have the other computer (LC2), but cannot log on with your laptop (invalid credentials).

There is a remote computer (RC1) that can log in to LC2.

Find passkeys for RC1 and LC2, allow LC1 to access LC2, plug in the flashdrive, and log in to LC2 to access the files and copy them to your computer - all within 20 minutes. Good luck!
