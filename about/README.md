# About
___

CodeViolin is a website created mainly for me to rant about random <i>stuff</i>. I will post on this website about my opinions, however crazy these ideas might be. I'll rant about everything from saving axolotls to warming freshwater lakes to the president's daily insanity. Essentially, it's a random blog. In addition, I will post book reviews. I am a voracious reader and I take in books at a rate of about 1 per day (longer depending on the length of the book). I won't post entire negative reviews, just a sentence with a rating.

I'm a coder who plays violin and my favorite animal is the axolotl.

Vio(lin>la)

Disclaimer: In case you're wondering, this website doesn't use trackers like Google Analytics. Visit my blog post [here](/blog/#google-part-2) about trackers.

Visit my [values/beliefs](values.html) page to learn more!

Thank you for visiting CodeViolin!

<a href="/easter-egg/" id="easter-egg">Easter Egg</a>
