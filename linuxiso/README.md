# Making a bootable Linux ISO
___


Note: This tutorial will not work for a Windows machine.

1. Get a flashdrive (4GB and up recommended)
1. Install UNetbootin:
  * `sudo apt install unetbootin`
1. Choose your distro and version
1. Make sure 'ISO' and not 'Floppy' is selected
1. Select USB or hard drive
1. Make sure your drive is selected (if you only have one flashdrive plugged in then it is automatically selected).
1. Click OK. At this point you might want to grab a snack or take a nap because this could take a while.
1. Once it's finished, you can test by rebooting your computer and using `F2`, `F10`, or `F12` to select boot method and select 'USB Drive'.
1. It should boot the distribution that you have selected.
