# Summer Camp: Web Development
___

Who: Isaac Barsoum, counselor. 6 students MAX.

What: A camp to learn basic HTML, CSS, and Pandoc Markdown web development.

When: July 2020. Exact week will be confirmed closer to the date. 

Where: Davidson Public Library (Location Subject to Change)/Davidson Green (Weather Dependent) 

Price: \$12/day/student, 5 days a week, 3 hrs a day (9-12)

Additional Costs:

* A laptop computer that can run Linux, Mac, or Windows.
* A CodeViolin t-shirt (\$24.99)

Curriculum

Day 1:

* Students will become acquainted with themselves, each other, and the counselor.
* All students will have a sufficient operating system *installed* on their computer.
* Students will understand basic principles of HTML.
* Students will learn how to set up an HTML skeleton page, including `<!DOCTYPE html>`, `<html>`, `<head>`, and `<body>` tags.
* Students will learn the `<p>`, `<h1>`, `<a>`, and `<img>` tags.
* Students will implement these tags into a basic website that will be previewed with BrowserSync, a tool that allows websites to be viewed before publishing.

Homework:

* Students will practice the tags they've already learned and learn more from the guide which will be provided.

Day 2:

* Students will learn more advanced web concepts, including semantics, `<title>`, `<blockquote>`, `<code>`, `<pre>`, and formatting tags including `<u>`, `<i>`, and `<strong>`.
* Students will implement these tags into their previously started website and preview it with BrowserSync without my assistance.
* Students will understand how to set up their own website and will begin creating a single page website on an animal of their choice.
  * Students will include at least one header, 2 subheaders, 3 links, 2 images, and 4 paragraphs in their website. 

Day 3:

* Students will understand basic CSS style rules, and how to learn more if they need them.
* Students will understand the [Box Model](/webtutorial/#lesson-4.1-the-box-model) and how to employ it in their website.
* All students will have a styled `<nav>` bar with multiple webpages.
* All students will understand `color`, `font-family`, `font-size`, `background-color`, `text-align`, `text-decoration`, `width`, and `height`, as well as `margin`, `border`, and `padding` as part of the Box Model.

Day 4:

* Students will learn basic markdown syntax including headings, paragraphs, links, and images.
* Students will begin to write a website in markdown.
* Students will learn the history of markdown and how markdown will be implemented into their website.
* Students will learn how using the terminal with markdown and CSS for web development is the most efficient way to learn web development.
* Interested students will begin to learn VIM for programming, and will continue by using vimgenius and vimtutor.


Day 5:

* Students will learn advanced markdown syntax including code, blockquotes, code blocks, comments, captions, lists (numbered and bulleted), and how to use HTML tags in their markdown document.
* Students will learn how to use Pandoc to convert their markdown pages into HTML
* Students will write a build script in bash (with guidance) to run each time they add to their website.

