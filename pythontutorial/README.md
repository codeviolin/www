# Python tutorial
___


Introduction: [Python](https://www.python.org) is a (programming) language used in data analysis and can be used for many other purposes. As we go along, you should follow along in a [REPL.it](https://repl.it) IDE (or another coding interface, if at home). In REPL, create a new account or log in to a previously created account. Click 'New Repl' in the top right corner and select 'Python' (you may have to search) as your language (not Python 2.7 or Python with Turtle. Just Python). Name the file 'python tutorial playground'. This will serve as our playground for the tutorial. Feel free to create as many files as you want (by clicking the file icon in the left sidebar) if you wish to save your code. [This description of coding](https://www.computersciencedegreehub.com/faq/what-is-coding/) is a lie. Congratulations! You are now ready to start coding!

* Note: as we go along, make sure to follow along in your own python3 file(s).
* Note: python2 syntax is very different from python3 syntax.

## Lesson 1: Setting up a basic python3 document

In the newly created document, use up a shebang line:

```
#! usr/bin/env python3
```

Now, write a line of code:

```
print('hello world')
```

Now, you have a basic python program! Click 'run' in REPL.it. 

### Lesson 1.1: Print statement 

The '`print`' statement allows you to print text to the console. Print syntax is as follows:

```
print('hello world')
```

* Note: In Python, quotations may be either double (" ") quotes or single (' ') quotes.

## Lesson 2: Lists

List syntax is thus:
```
YOURLISTNAME = [
    "YOURLISTITEM",
    "ANOTHERLISTITEM",
    "LASTLISTITEM",
]
```
Lists can also be one line:
   ``` 
   LIST2 = ["Hello there", "Goodbye",]
   ```

* Note: Use regular brackets (`[]`) rather than curly brackets (`{}`).

Lists can be referenced in variables (lesson 6) by the list name.

Lists can have an unlimited number of items. 

## Lesson 3: Arguments 

Arguments are the values passed through the function when the function is being used. These can vary, such as different strings in a `print()` function, and varying seconds values in `time.sleep()` functions.

## Lesson 4: Modules

Python comes with lots of built-in modules, including `random`, `math`, `os`, `sys`, and `time`. Use these modules by 'importing' them with the following syntax:

```
import random
import math
import os
import sys
import time
```

* Tip: Put all imports at the top of files.

Find the functions that these modules provide with the `dir()` function:

```
#!/usr/bin/env python3
import math
content = dir(math)
print(content)
```

* Easter egg: type '`import this`' into the playground and run. 

### Lesson 4.1: random module 

The random module allows you to select a random string from a list, as well as other things.

```
import random
YOURLISTNAME = [
    "YOURLISTITEM",
    "ANOTHERLISTITEM",
    "LASTLISTITEM",
]
choice = random.choice(YOURLISTNAME)
print(choice)
```

This will print a random list item, choosing from the three items in the existing list.

### Project - Random Greeting

Create a project that, when run, prints a random greeting. This should use lists and the random module. Make sure to use valid syntax and include more than 5 greetings. 

### Lesson 4.2: math module 

The math module allows you to generate a number between 0 and 1, to round that number, to floor that number, to raise that number, to do arithmetic, etc. [Math is WRONG in py2](https://goo.gl/kdkfGj).

This module is possibly the most important in data science.

Basic operators:

```
total = 5 + 10          # addition
difference = 10 - 5 	# subtraction
product = 10 * 5        # multiplication
quotient = 10/5         # division
power = 10**5           # exponents
remainder = 10%3        # remainder
```

* `+=` (`a += b` is equivalent to `a=a+b`), 
* `-=` (`a -= b` is equivalent to `a=a-b`), 
* `*=` (`a *= b` is equivalent to `a=a*b`), 
* `/=` (`a /= b` is equivalent to `a=a/b`), 
* `**` (exponent (`a**b` is `a` to the power of `b`)), 
* `%` (remainder (`a%b` returns remainder of `a/b`)),
* `>` (greater than),
* `<` (less than), 
* `>=` (greater than or equal to),
* `<=` (less than or equal to)

### Example: dice roller

```
#!/usr/bin/env python3

from random import random
import math

def rollone(sides):
    R = random()
    rolled = sides * R + 1
    rolled = math.floor(rolled)
    return rolled

def roll(num,sides):
    count = 0
    total = 0

    while count < num:
        count += 1
        total += rollone(sides)
    return total

def rolly():
    print(rollone(6))

rolly()
```
This short program is rolling a six-sided die using the math module and the random module. 

* Note: `print(rollone(6))` multiplies `rollone` times 6, giving a 6 sided die. 

### Lesson 4.3: os module 

I use the os module for 1 thing: clearing the terminal while person is in gameplay.

```
import os
clear = lambda : os.system('tput reset')
```

Use the 1-liner (lambda) and then use `clear()` when you wish to utilize the function.

### Lesson 4.4: sys module

Also, I use the `sys` module for only one thing: making it look like a `print("string")` is being typed rather than just popping up on the screen.

```
import sys
import time
def write(string):
    for char in string:
        sys.stdout.write(char)
        sys.stdout.flush()
        time.sleep(0.1)
    print("")
```

Use `write("YOURTEXTHERE")` when you want to use this.

### Task - Testing sys and os modules

Use the `os clear()` and the `sys write(string)` functions to create a program that writes 'Hello world' and then clears it. Adjust time in between printing of characters (or better yet, make it inconsistent (`random`) so that it really seems like someone's typing), play with the `lambda` to expand it to multiple lines, or change the argument for `os.system` in the `lambda` to see if other arguments can be passed with valid syntax (hint: there is one other).  

### Lesson 4.5: time module 

I use the time module when wanting to allow people to read what I've written with the `write(string)` function utilizing the `sys` module before clearing the terminal with the lambda from the `os` module.

After defining the `write()` and `clear()` functions, do this in your program:

```
import time
```

[this is where the `write()` and `clear()` functions go]

```
write("Hello world")
time.sleep(3) 
#the argument is the number of seconds to 'wait'.
clear()
```

This will write 'Hello world', wait for the argument given (3 seconds) and then clear the terminal.

The time module also allows you to print the current time when the program is run:

```
#!/usr/bin/env python3
import time

time = time.ctime()
print(time)
```

This prints: `Sat May 11 22:51:30 2019` 

* Note: [Greenwich Mean Time](https://greenwichmeantime.com/what-is-gmt/) (prime meridian) 

## Lesson 5: Value Types 

The different value types in python include:

* strings, which are used as text, numbers,
* long strings, which are just multi-line strings,
* numbers, which are used as, well, numbers, and
* variables, which contain strings or numbers. (lesson 6)

String syntax: 
```
"Hello world"
```

Long string syntax:

```
greetings = 
"""
Hello
Goodbye
"""
```
Long strings have to be assigned to variables.

Number syntax: `5`

* Note: Numbers DO NOT use quotes, EVER! In any language.

## Lesson 6: Variables and Inputs

Variables allow for the naming of a variable, such as 'name'. The syntax for a variable with a string value is as follows:

```
name = "YOURNAMEHERE"
```

Inputs allow you to have the user input a string and it to be fed into a variable.

```
name = input("What is your name? ")
```

Using the input does not require you to `print(name)`. It will automatically appear when you run the program. Using `print(name)` will print the inputted value. Therefore,

```
name = input("What is your name? ")
print(name)
```

Prints the string that the user inputted.

Using the `+` operator, we can add our own strings plus the user's input.

```
name = input("What is your name? ")
print("Nice to meet you, " + name)
```

This prints: Nice to meet you, [USER INPUT HERE].

## Lesson 7: If/elif and if/else statements

`If/elif` statements allow you to scan a string or number and perform an action `if` the string or number is this, or if it is this other thing (`elif`), do this. `If/else` statements allow you to scan a string or number and perform an action `if` the string or number is this, or if it is anything else (`else`), do this.

### Example 1: If/elif:

```
name = input("What is your name? ")
if name == "bob":
    print("That's a cool name, bob! That's my name too!")
elif name == "Bob":
    print("That's a cool name, Bob! That's my name too!")
```

* Disadvantage: This program will end if you do not input 'bob' or 'Bob'.

### Example 2: If/else:

```
name = input("What is your name? ")
if name == "bob":
    print("That's a cool name, bob! That's my name too!")
else:
    print("Nice to meet you, " + name + "! My name is bob!")
```

* Advantage: Does not end the program if your name is not Bob.

### Example 3: If/if/else:

```
name = input("What is your name? ")
if name == "bob":
    print("That's a cool name, bob! That's my name too!")
if name == "john":
    print("That's a cool name, john! I have met someone named john before!")
else:
    print("Nice to meet you, " + name + "! My name is bob!")
```

* Advantage: Allows for a more interactive interface. 
* Note: If/elif/else is invalid syntax. Use If/if/else instead.

### Lesson 7.1: while statement

```
x=5
y=1

while x>y:
    print("Hello")
    y+1
while x=y:
    print("Goodbye")
    exit()
```
`exit()` is used at the end of a program to exit the user back into the bash interface.

## Lesson 8: Functions

Functions are used to prevent [spaghetti code](https://sourcemaking.com/antipatterns/spaghetti-code) and organize a file into sections. Function syntax:

```
def welcome():  
    name = input("What is your name? ")
    print("Hello, " + name +", nice to meet you!")
    after()

def after():
    print("This function comes after the welcome function.")

welcome() #The starting function - change if you want to change the starting function
```

This outputs:

```
What is your name? Bob
Hello, Bob, nice to meet you!
This function comes after the welcome function.
```
Functions are very useful, especially if you want to save game information or organize data into sections.

## Lesson 9: Comments

Comments are very useful when you are sharing code or collaborating on a project. Comment syntax is:

```
welcome() 
# The starting function - change if you want to change the starting function
```

Comments begin with a hashtag. 

* Note: In python, there is no way to make multi-line comments.

## Project - Developing an intermediate python project

For your project, you will be creating a simple text adventure game. There must be at least 4 different locations. These locations can include battles, lessons at school, etc. Be imaginative! You must include a __shebang line__, __functions__ with __arguments__, __inputs__, __variables__, __strings__, at least one __module__, at least one __comment__, at least one __if/else__ or __if/elif__ statement, and you may use the `os clear()` function, the `sys write(string)` function, __lists__, __numbers__, and __while__ statements (you may also use previous python knowledge to enhance the game). Your game must have a __clear theme__ and must allow users to __win__ or __lose__. You must use __correct syntax__ and the game must operate correctly no matter what the user inputs. TEST YOUR PROJECT!!! Knowledge from the first 7 lessons should be sufficient to allow you to make an intermediate level game. Good luck!

* Note: Remember, it is not shameful to look up syntax if you don't know it and it's not in this document. You will always have a computer to look something up on if you are programming. Make sure the syntax is correct for python3.
* This [text adventure easter egg](https://techcrunch.com/2018/10/01/theres-a-secret-text-adventure-game-hidden-inside-google-heres-how-to-play-it/) was made by Google. It is a good premise for a game, although theirs is very complicated and has many locations.
* The easter egg can be accessed by searching text adventure in Google (or<i>!g text adventure</i> in <i>[DuckDuckGo](https://www.duckduckgo.com)</i>) and opening Chrome DevTools by right clicking to 'Inspect' or 'Inspect Element' and navigating to the 'console' tab.
    * Tip: Use DuckDuckGo at home (blocked at school 😠) instead of Google!
        * [spreadprivacy.com](https://spreadprivacy.com)
    * The easter egg will not work on school chromebooks where inspect element is disabled.
* [Here](https://en.wikipedia.org/wiki/List_of_Google_Easter_eggs) is a list of google easter eggs
    * [Easter egg](https://www.google.com/search?q=define+easter+egg&rlz=1CAPQVW_enUS848&oq=define+easter+egg&aqs=chrome.0.69i59.2091j0j7&sourceid=chrome&ie=UTF-8&safe=active&ssui=on): technical jargon for an unexpected or undocumented feature in a piece of computer software or on a DVD, included as a joke or a bonus.  

## Lesson 10: .lower() 

Python is rather aggravating when inputs are being used (case-sensitivity). Luckily, there is an easy solution:

```
variable = input("Type something here: ")
variable = variable.lower()
print(variable)
```

This outputs:

```
Type something here: Hi
hi
```

Therefore, you do not need to account for all types of upper- and lower- case potentialities. Instead, use `.lower()`!

* Now, if necessary, simplify your project so that `.lower()` is used rather than if/elif/else being used to account for all possibilities.

## Lesson 11: The Complex Dictionary

In gameplay, the storage of information is key to creating an interactive interface. The dictionary can contain different tidbits of information that have been collected, such as Hogwarts 'Houses', name, highscores in a game, interests, answers (quiz game), age, etc.

* A big disadvantage of python: any input within a function cannot be called outside of that function.

Dictionary example:

```
student_name = input("Name: ")
student_age = input("Age: ")
student_house = input("Hogwarts House: ")

student = {
	'name': student_name,
	'age': student_age,
	'house': student_house,
}

print("Hello there. Nice to meet you, " + student.name + "! You are already " +
student.age + " years old? I am also in " + student.house +"!") 
#This line was used as an example for calling each item from the dictionary. 
```

* Note: json dictionaries are much more effective than python ones.
* Note: Use `listname.item` to call each item (just `house` or `name` doesn't work.)
* Note: Redefine dictionary items by calling `student.name`.
