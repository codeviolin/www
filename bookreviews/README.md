# Book Reviews
___

Ratings out of 5 stars.

## Warning: Potential Spoilers

## Scythe

*Scythe* by Neil Shusterman is an exceptional dystopian young adult novel. Citra Terranova and Rowan Damisch, two teens in a post-mortal age, embark on a journey that turns them into **scythes**, or life-takers. The scythes are the only population control method in the world, and live by a code of honor that enforces unbiased killing, or "gleaning." However, when the Scythedom is corrupted, the new-order, bloodthirsty scythes threaten all that the Scythedom stands for. Will Rowan and Citra prevail and uphold honor? Or will the new order bring a new era of darkness to death? Find out in this stellar novel, its companion novel *Thunderhead* and its future companion novel *The Toll* (release date: 10/05/2019).

(4.7 stars)


## Shakespeare

### Read more [here](https://www.theatlantic.com/magazine/archive/2019/06/who-is-shakespeare-emilia-bassano/588076/), [here](http://www.shakespeare-online.com/plays/topplays.html), [here](https://www.folger.edu/shakespeare) and [here](https://en.wikipedia.org/wiki/William_Shakespeare).


### The Taming of the Shrew

*The Taming of the Shrew* is a complicated comedy about an Italian father, his two daughters, and their suitors. The father, Baptista, insists that his younger daughter, Bianca, cannot marry until his older daughter, the "*Shrew*", Katherine, does so. However, Bianca feels that Katherine might never marry. When Gremio, Hortensio, and Lucentio arrive with their servants, looking to marry Bianca, they must do so in secret. Lucentio and Tranio, one of his servants, switch clothes so that Tranio can distract Baptista in order for Lucentio, in disguise as the teacher Cambio, may woo Bianca. Hortensio arrives in disguise as Litio, a music teacher. Petruchio, lover of Katherine, arrives and intends to marry her. Lucentio outbids Hortensio for Bianca, and Petruchio marries Katherine. Petruchio intends to tame Katherine the way a trainer would tame a falcon. What will the couples' fates be? Find out in this play.

(4.3 stars)



### King Lear

*King Lear* is a tragedy about a person of the same namesake. When King Lear of Britain intends to divide his power among his three daughters, Goneril, Regan, and Cordelia, he requires that they pledge their love to him. All three answer falsely, Goneril and Regan pledging their love and Cordelia refusing. In another part of England, the Duke of Gloucester is tricked by his son, Edmund, that his other son, Edgar, is plotting to kill him. Edmund intends to become the Duke's heir. When Goneril and Regan turn against him, Lear finds refuge with Cordelia and the French army. Gloucester goes to find Lear, who is alone in a storm. The Duke of Cornwall blinds Gloucester, whom he believes is a traitor. What will be the fate of King Lear, Edgar, Edmund, Gloucester, Goneril, Regan, and Cordelia? Find out in this tragedy.

* Note: The plot is rather difficult to follow in this all-over-the-place play. You have been forewarned.

(4.3 stars)


### Othello

*Othello* is a dark tragedy in which Othello, a Venetian general, is framed by Iago, a hater of Othello, into taking Desdemona, the daughter of a Venetian senator (Brabantio), from her house. Brabantio forms an band of people to punish Othello. As Othello prepares to fight a Turkish fleet, unbeknownst to this, Desdemona pledges her allegiance to Othello, causing Brabantio to think that Othello used a 'love potion' or something of the sort. Othello is tricked by Iago into falsely believing that Desdemona is in a relationship with Cassio, Othello's lieutenant. Othello becomes enraged at Desdemona and Cassio and appoints Iago as lieutenant in Cassio's place. Iago begins to plot his rise to power from the inside. What will be the fate of Othello and Desdemona? Find out in this play.

(4.4 stars)

### Much Ado About Nothing

*Much Ado About Nothing* is a good play featuring much ado about, well, nothing. Count Claudio, hero of a recent war under Don Pedro, falls in love with the daughter of the Governor of Messina, Hero. He asks Don Pedro to woo her for him. The Governor, given a misinterpreted account of the conversation between Claudio and Pedro, believes that Don Pedro wishes to marry Hero. Don Pedro's brother, Don John, loser of the war between the two brothers, intends to stop the marriage. Don John intends to trick Count Claudio into believing that Hero is unfaithful. Don John's followers trick Count Claudio into believing the ruse. What will the fate of Claudio and Hero be? Find out in this play.

(4.4 stars)

### The Merchant of Venice

*Merchant* is a relatively dull play when compared to other Shakespearean works, but it is still not to be ignored. A Venetian named Bassanio desires Portia, living at her estate of Belmont. He needs money to travel to Belmont, and so asks his friend, Antonio, a merchant, for money. Antonio has invested all his money in trading expeditions, but trusts that he will be able to repay the debt, so he agrees to allow Bassanio to borrow the money in his name. When the trading ships wreck, Shylock, the moneylender, declares that he will take a pound of Antonio's flesh if the loan is not paid off. What will Antonio's fate be? Find out in *The Merchant of Venice*.

(4.2 stars)


### Julius Caesar

*Julius Caesar* is a tragic tragedy about the weight of good and evil, the choice between a dastardly deed and injust rulership, and the Roman empire. Caesar is wanted by the people as the Emperor of Rome, but several "conspirators" instead decide that if Caesar becomes emperor, the fate of the people will be affected for the worse, and therefore decide to attempt an assassination. Brutus (a conspirator) questions his decision, especially because he is not making this decision out of hate or a grudge. He is committing this treachery for the greater good. *Beware the Ides of March* but don't beware of this play.

(4.5 stars)

### Antony and Cleopatra

*Antony and Cleopatra* is a good play, but (I learned this the hard way) should be read **after***Julius Caesar*. Without reading *Julius Caesar*, it is difficult to understand. Nonetheless, it is a play worthy of your time. Set in the Mediterranean area (Egypt, Greece, Italy, etc.), it tells the story of Antony (a trimuvir of Rome) and Cleopatra's (the Queen of Egypt) complicated relationship. The Emperor of Rome (Octavius Caesar) disapproves and orders Antony to return to Rome. When he doesn't, Octavius sends messengers and soldiers to forcefully bring him back (if necessary). Cleopatra feels betrayal when Antony consequently marries Octavius' sister, Octavia. But when things get tense between three Mediterranean forces, will Antony put Cleopatra or his power first? Find out in this tragedy.

(4 stars)

### The Comedy of Errors

*The Comedy of Errors* is an exceptionally witty comedy written by William Shakespeare. It is one of his two plays (The Tempest) to roughly adhere to the [Aristotelian unities](https://en.wikipedia.org/wiki/Classical_unities) - the unities of time and place are most recognizable here. When Antipholus of Syracuse and his servant, Dromio of Syracuse, travel to Ephesus in search of Antipholus' wife and children, confusion runs wild when they become tangled up with their twin counterparts - Antipholus and Dromio of Ephesus. Each Antipholus expects his Dromio to perform a task for him, but they instead perform a task for the other Antipholus. This brings Antipholus of Syracuse gold and riches, and Antipholus of Ephesus a rope. Will the confusion spark unrest and more confusion? Or will they resolve this problem by sorting out their confused selves? Find out in this short and sweet comedy.

(4.8 stars)

### The Tempest

*The Tempest* by William Shakespeare is a complicated, exceptional, comitragic play that is certainly not lost "*in the dark backward and absym of time*" [Prospero, 1.2.50]. Prospero, a wizard, the rightful Duke of Milan, exiled to a desolate island, calls up a tempest when the King of Italy, the serving Duke of Milan (twist: also Prospero's brother), the King's son, and their comrades approach the island. Prospero, a questionable and unsteady character, a hero and villain at the same time, is sought by these people. Prospero places them under his control by charming them, but two get away and join forces with a lonely inhabitant of the island. They seek Prospero with a desire to kill him and rescue their associates. Will they succeed in their mission? Or will Prospero prevail and bring their downfall? Read *The Tempest* to find out.

(4.8 stars)

### Macbeth

*Macbeth* by William Shakespeare is a stellar tragedy. Focusing in on the Thane of Glamis (Macbeth), who kills the Thane of Cawdor and inherits the position. When King Duncan comes to celebrate with him, Macbeth, who has a deep desire to become king, is overwhelmed by his desire and proceeds to assassinate the king. Killing many others (and framing others, too) along the way, Macbeth becomes king. As the story develops, Macbeth delves into a mental catastrophe about right and wrong. In the end, will he be found out and killed? Or will he escape once more and rule Scotland? Find out in this exceptional play.

(4.9 stars)

___

## It Ain't So Awful, Falafel

*It Ain't So Awful, Falafel* is a wonderful book about an Iranian girl who is living in the US for her dad's work. A brainiac who tries desperately (and fails miserably) to get her mom to learn English, she is forced to fare for herself in Newport Beach. At school, she has friends, but her home life is eating, watching TV, and sleeping. When the Iranian Revolution begins, she begins to fear for her friends in Iran. Inevitably, she will have to move away, back to Iran, half a world away. But in the current social and political climate there, she doesn't want to. What will happen to Zomorod? Find out in this stellar novel by Firoozeh Dumas!

(4.7 stars)

## All American Boys

A *profound* look on the state of police brutality in the US, *All American Boys* zooms in on two perspectives - one black (Rashad), one white (Quinn), both American - to tell the story of Rashad Butler (one of the points of view), who is beaten up by a white policeman (Quinn's friend's brother). The high school which Butler attends and the community in which he resides rise with him, protesting police brutality, which in some scenarios, is completely unnecessary. Quinn must make the decision whether to stand with the movement or to stand with his friend in a time when his friend needs him the most. *All American Boys* is a must-read and will merit the first five-star rating on the page.

(5 stars)



## One-Third Nerd

Horrible. I would advise against reading. By Gennifer Choldenko.

(0.6 stars)


## Front Desk

As Mia Tang juggles racism, the front desk of a small motel in Anaheim, and an unkind supervisor, she must learn to fight for what is right. Through letters, essays, and feedback cards, Mia learns that a small voice can be heard a long way away. With a desire to be a writer, her mom tells her that she is "a bicycle, and all the other kids are cars," in reference to her English ability. But Mia manages to convince lots of people to donate money in order to buy the motel so that others can have opportunities in America. Read this stellar novel (*Front Desk*) by Kelly Yang!

(4 stars)


## The Benefits of Being An Octopus

*The Benefits of Being An Octopus* is an eye-opening book encircling the struggles of poverty, the stresses of a dysfunctional relationship, and the managing of small children at a young age. Told from the point of view of a young girl in Vermont living in a trailer park, *The Benefits of Being An Octopus* is a must-read and a potential future classic. With struggles at school with her friend (yes, not friend*s*), taking care of a 4-year-old, a 3-year-old, and an infant, and struggling to help her mom get past her unkind boyfriend, Zoey sometimes has to be an octopus without being an octopus. 
"A compassionate look at poverty, hard choices, and defending one's right to be treated humanely. A very fine first novel, written with a deft hand." - Karen Hesse, Newbery Award-winning author of Out of the Dust. Written by Ann Braden, a budding writer extraordinaire.

(4 stars)


## Breakout

*Breakout* is an enlightening book emphasizing friendship, racism, and a small community standing together. When two inmates break out of a high-security prison, a manhunt is launched to find them. The manhunt disrupts the lives of all the citizens of Wolf Creek, the upstate New York town where the prison is located. Composed of letters, text messages, diagrams, recorded conversations, poems, news stories, and comics, *Breakout* is a fast-paced, thought-provoking story. Written by Kate Messner.

(4.5 stars)



## The Miscalculations of Lightning Girl

This is my first book review for my website, so here goes:

*The Miscalculations of Lightning Girl* is a spectacular novel written by Stacy McAnulty. It focuses on the main character, Lucille (Lucy) Callahan, who was struck by lightning at age 8 - and possesses genius-level math skills and is well above-average in all subjects - all caused by the lightning strike. There is one exception to her array of abilities: her social skills. Lucy has never been to middle school, and when her grandmother decides to send her there, her undeveloped social courtesies are put to the test. At school, people think she's odd (and some make fun of her for it), because the lightning also possessed her with Obsessive-Compulsive Disorder (OCD). The OCD causes her to perform certain unordinary actions. Can Lucy overcome her differences and survive 1 year of middle school? Find out in this stellar novel!

(4 stars)

