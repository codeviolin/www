# Turning off Google Analytics in Google Drive
___

To turn off Google Analytics in Google Drive, do the following:

* Open a new google document.
* Click the line graph icon in the top right corner to the left of the 'comments' button.
* Go to the 'Privacy settings' page.
* Turn off 'Show my view history for all Docs, Sheets, and Slides files'.
* Done!

Also, I suggest using the [DuckDuckGo Privacy Essentials Extension](https://chrome.google.com/webstore/detail/duckduckgo-privacy-essent/bkdgflcldnnnapblkhphbgpggdiikppg), which blocks unfriendly trackers. (And, of course, is blocked on school chromebooks). Also, I suggest using the [DuckDuckGo](https://www.duckduckgo.com/) search engine in lieu of Google, and reading the DuckDuckGo blog. Also, read [my blog](https://www.codeviolin.me/blog/) about Google Chrome.