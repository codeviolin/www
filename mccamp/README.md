# Summer Camp: Systems Administration with Minecraft 
___

Who: Isaac Barsoum, instructor. 5 students MAX.

What: A camp for kids and teens to set up their own Minecraft server.

When: June 2020. Exact week will be confirmed closer to the date. 

Where: Davidson Public Library (Location Subject to Change)/Davidson Green (Weather Dependent)

Price: \$12/day/student, 5 days in the week, 3 hours a day (9-12)

Additional Costs:

* Laptop computer running or can run Mac, Linux, or Windows (required), to bring to camp each day OR \$100 pre-paid to purchase a Raspberry Pi, case, and micro SD card.
  * Raspberry Pi would allow students to take home their own computer running a Minecraft server that they can use with their friends. Raspberry Pi would also allow them to tinker and learn bash, systems administration, web development, and more on their own. 
* Minecraft account (optional, only if you want to play with others) (\$26.95)
* CodeViolin t-shirt (optional, \$24.99 [subject to change])

Curriculum

Day 1:

* All students will have a computer that now runs Mac, Linux, or Windows.
* All students will install WorldEdit and Spigot, two tools that are necessary for a Minecraft server.
* All students will install Minecraft on their laptop computers and will become familiar with the controls.
* All students will have a `mc` directory in which to place their files.

Day 2:

* All students will become oriented with the bash terminal's basic commands and how to use it to begin their Minecraft server.
* All students will become familiar with some of the essential Minecraft console commands, including `op`, `gamemode`, and `help`.
* All students will learn how to start their server from the web and invite their friends to play.

Day 3:

* All students will begin their Minecraft servers and will become oriented with how the game works and how it relates to systems administration.
* All students will learn more necessary bash commands.
* All students will become familiar with the in game commands after they become an operator on their server.

Day 4:

* All students will now be able to accurately start their Minecraft servers.
* All students will play on each other's Minecraft servers.
* All students will understand systems administration and how it is used in other ways.
* All students will understand how the World Wide Web connects players to a server.

