# Tutorials
___

[Front-end web tutorial](/webtutorial/)

[Python tutorial](/pythontutorial/)

[Markdown tutorial](/markdowntutorial/)

[Sourcing a website through a Git client](/gitclone/)

[Creating a bootable Linux ISO](/linuxiso/)

[Disabling analytics in Google Drive](/analyticsoff/)

[Lynx Configuration](/blog/#use-lynx-terminal-web-browser)

[Cutting off your Google Use](https://www.codeviolin.me/blog/#privacy-violating-privacy-policies)

