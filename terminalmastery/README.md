# Terminal Tutorial
___

Introduction: This tutorial will walk you through the basics of configuration, orientation, and use of the essential terminal utilities: bash, lynx, vim, and tmux. Each of these tools has a specific purpose and each is necessary to becoming a terminal master. Every computer worth buying comes with a built-in UNIX based bash shell environment which makes development that much faster. All that's left to do is learn how to use it.

## Lesson 1: The Tools

Bash is the standard shell language. Commands within the terminal itself are given in bash. Vi/Vim is the terminal editor. It can be used as the primary code editor on any device and uses specific keystrokes to carry out specific commands. TMUX is the terminal version of multitasking. With many functions including time display and continual execution on top of the split-screen display, TMUX is a coder's dream.  
