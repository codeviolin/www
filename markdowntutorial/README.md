# Markdown tutorial
___

> Disclaimer: I highly suggest doing the [Web tutorial](/webtutorial/) before this tutorial.

Introduction: Markdown is the easiest programming language you'll ever learn, and yet it's necessary for creating and publishing your website. If I told you my website was written completely in Markdown and CSS, would you believe me? This tutorial will walk you through markdown syntax and how to employ markdown to deploy a website. (See what I did there?). Please follow along and practice your markdown as you go on.

## Lesson 1: Headers and Paragraphs

Markdown headers are indicated with hashtags. An `<h1>` element in HTML is equivalent to 1 hashtag in markdown. An `<h2>` element in HTML is equivalent to 2 hashtags, and so on. The syntax for a header would be:

~~~markdown
# I am a header
~~~

Paragraphs are simply text with at least 1 line between elements above and below:

~~~markdown
# I am a header

This is a paragraph

## Another header
~~~

## Lesson 2: Links, Images, and Videos

The syntax for images and links is very similar.

For links:

~~~markdown
[Link text](https://www.codeviolin.me)
~~~

For images:

~~~markdown
![Image Caption](image.png)
~~~

Inside the brackets, place the caption that will turn into a `<figcaption>` if you're using Pandoc. This caption will also be used as `alt` text.

Videos are just an image nested within a link (clicking will take user to the webpage where the video is located, there is no way to embed videos using markdown):

~~~markdown
[![Everything Is AWESOME]
(https://img.youtube.com/vi/StTqXEQ2l-Y/0.jpg)]
(https://www.youtube.com/watch?v=StTqXEQ2l-Y "Everything Is AWESOME")
~~~

Source: [Stack Overflow](https://stackoverflow.com/questions/14192709/is-it-possible-to-embed-youtube-vimeo-videos-in-markdown-using-a-c-sharp-markdow)

Use the thumbnail image from YouTube as the link for the image, and the video URL as the link for the, well, *link*.

## Lesson 3: Lists

The syntax for bulleted lists is simply a star/asterisk before each item:

~~~markdown
* List item 1
* List item 2
~~~

The items here require a space between the asterisk and the item to be considered a list item. This will turn into `<ul>` and `<li>` using Pandoc.

Output (when syntax is fixed):

* List item 1
* List item 2


The syntax for numbered lists is simply a `1.` before each item:

~~~markdown
1. Best thing EVER
1. Second best thing EVER
1. Third best thing EVER
~~~

Output:

1. Best thing EVER
1. Second best thing EVER
1. Third best thing EVER

## Lesson 4: Formatting

*Italic* font:

In markdown, italic font is marked by two asterisks placed on either side of the italicized string:

~~~markdown
*This is italicized font*
~~~

*This is italicized font*

~~~markdown
**This is bold font**
~~~

**This is bold font**

~~~markdown
***This is bold and italicized font***
~~~

***This is bold and italicized font***

~~~markdown
`This is monospaced (code) text`
~~~

`This is monospaced (code) text`

## Lesson 5: Blockquotes/Code Blocks

Blockquotes, or blocks, are shown with the key `>`. Before every paragraph that is in a blockquote, add a `>`.

~~~markdown
> This is a blockquote.
~~~

Multi-line paragraphs require only 1 blockquote.

Code blocks are another feature of markdown. Stylized with the specific programming language, these can be customized with a few keystrokes.

~~~markdown
  ```html
  <p>Hello world</p>
  ```
~~~

Results in:

```html
<p>Hello world</p>
```

All modern programming languages are supported and can be used through their abbreviations that are used in their filenames (i.e. `css`, `js`, `rust`, etc.)

## Lesson 6: Integration with HTML

All HTML tags are supported in Markdown as well, so whenever you have that time where you forget how to do something, you can temporarily replace it with an HTML tag.

## Lesson 7: Pandoc

*Pandoc* is an extremely useful tool used to convert Markdown pages into HTML files for the web. To use Pandoc, simply:

1. Create a Markdown document
2. Terminal command:

```bash
sudo apt install pandoc
```

3. Copy my [build file](/build/) and my [template file](/template/) into the directory in which your website is located (or create your own using the [Pandoc documentation](https://pandoc.org/). The template file is simply a skeleton file.
4. Within your `functions.bash` file within your [config](https://gitlab.com/robmuh/dotfiles) directory, add the following function:

```bash
send () {
  cd
  cd YOUR WEBSITE DIRECTORY
  ./build
  git add .
  git commit
  git push
}
```

Replace YOUR WEBSITE DIRECTORY with the location of your directory.

For this, your website must be [cloned](/gitclone/) in (preferably) GitLab, or (not preferably) GitHub.


## Project: Website

Create a directory in which you have multiple Markdown files. Use Pandoc strategies to effectively manage the website.

You must have one of each of these:

* a homepage
* a CSS file ([learn how to do this here](/webtutorial/))
* a `template.html` file
* a `build` file
* three or more of the following content pages (or come up with your own):
  * blog
  * book reviews
  * news
  * about
  * games
  * opinions 


Congratulations! You have completed the Markdown tutorial.
